import prisma from "../lib/prisma";
import { promises as fs } from "fs";

export const resolvers = {
  Query: {
    getUsers: async () => {
      const users = prisma.user.findMany({
        orderBy: [{ name: "asc" }],
      });
      return users;
    },
    getEntries: async (_, { day }) => {
      const entries = await prisma.entry.findMany({
        where: {
          day: {
            date: day,
          },
        },
        include: {
          user: true,
        },
      });
      return entries;
    },
    getMyEntries: async (_, { user }) => {
      if (!user) return [];

      let yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);

      const entries = await prisma.entry.findMany({
        where: {
          user: {
            name: user,
          },
          day: {
            date: {
              gt: yesterday,
            },
          },
        },
        include: {
          day: true,
        },
      });

      return entries;
    },
    getChangelog: async () => {
      const changelog = await fs.readFile(
        process.cwd() + "/CHANGELOG.md",
        "utf8",
      );
      return changelog;
    },
  },
  Mutation: {
    addUser: async (_, { name, isPermanent }) => {
      const existing = await prisma.user.findUnique({
        where: { name },
      });
      if (existing) {
        return false;
      }

      await prisma.user.create({
        data: {
          name,
          isPermanent,
        },
      });

      return true;
    },
    deleteUser: async (_, { name }) => {
      await prisma.user.delete({
        where: { name },
      });

      return true;
    },
    updateEntry: async (_, { day, user, attendance }) => {
      const existing = await prisma.entry.findFirst({
        where: {
          day: {
            date: day,
          },
          user: {
            name: user,
          },
        },
      });

      if (existing) {
        await prisma.entry.update({
          where: {
            id: existing.id,
          },
          data: {
            attendance,
          },
        });
        return true;
      }

      await prisma.entry.create({
        data: {
          attendance,
          user: {
            connect: {
              name: user,
            },
          },
          day: {
            connectOrCreate: {
              where: {
                date: day,
              },
              create: {
                date: day,
              },
            },
          },
        },
      });

      return true;
    },
  },
};
