import { DateTime } from "graphql-scalars";

export const typeDefs = `
  scalar DateTime

  type User {
    name: String
    isPermanent: Boolean
    entries: [Entry]
  }

  type Day {
    id: Int
    date: DateTime
    entries: [Entry]
  }

  type Entry {
    id: Int
    day: Day
    user: User
    name: String
    attendance: Int
  }

  type Query {
    getUsers: [User]
    getEntries(day: DateTime): [Entry]
    getMyEntries(user: String): [Entry]
    getChangelog: String
  }

  type Mutation {
    addUser(name: String, isPermanent: Boolean): Boolean
    deleteUser(name: String): Boolean
    updateEntry(day: DateTime, user: String, attendance: Int): Boolean
  }
`;
