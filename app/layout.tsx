"use client";

import Environment from "@/components/environment";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <title>Zobig</title>
        <link rel="icon" href="/favicon.svg" type="image/svg" />
      </head>
      <body>
        <Environment>{children}</Environment>
      </body>
    </html>
  );
}
