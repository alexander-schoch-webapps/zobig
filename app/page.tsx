"use client";

import { useState, useContext, useEffect, FormEvent } from "react";
import { ColorContext } from "@/components/environment";

import {
  ActionIcon,
  Alert,
  Badge,
  Box,
  Button,
  Card,
  Center,
  Checkbox,
  Chip,
  ColorSwatch,
  Container,
  Group,
  Grid,
  Indicator,
  Modal,
  Select,
  Space,
  Text,
  TextInput,
  useMantineTheme,
  useMantineColorScheme,
  MantinePrimaryShade,
} from "@mantine/core";

import {
  IconArrowRight,
  IconArrowLeft,
  IconCalendar,
  IconUserPlus,
  IconUserMinus,
  IconCheck,
  IconX,
  IconQuestionMark,
  IconMicrowave,
  IconDevicesOff,
} from "@tabler/icons-react";

import { DatePicker, DatePickerProps } from "@mantine/dates";

import { UserType, EntryType } from "@/types/types";

import { useQuery, useMutation, gql } from "@apollo/client";

const addUserMutation = gql`
  mutation addUser($name: String, $isPermanent: Boolean) {
    addUser(name: $name, isPermanent: $isPermanent)
  }
`;

const deleteUserMutation = gql`
  mutation deleteUser($name: String) {
    deleteUser(name: $name)
  }
`;

const getUsersQuery = gql`
  query getUsers {
    getUsers {
      name
      isPermanent
    }
  }
`;

const updateEntryMutation = gql`
  mutation updateEntry($day: DateTime, $user: String, $attendance: Int) {
    updateEntry(day: $day, user: $user, attendance: $attendance)
  }
`;

const getEntriesQuery = gql`
  query getEntries($day: DateTime) {
    getEntries(day: $day) {
      attendance
      user {
        name
      }
    }
  }
`;

const getMyEntriesQuery = gql`
  query getMyEntries($user: String) {
    getMyEntries(user: $user) {
      day {
        date
      }
    }
  }
`;

export default function Home() {
  let today = new Date();
  /*
   * This date will be wrong anyway, as this is calculated at build time.
   * Therefore, we set it to one year before, such that the DatePicker does not
   * display the wrong date.
   */
  today.setFullYear(today.getFullYear() - 1);
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);
  const [date, setDate] = useState<Date>(today);
  const [addUserModalOpen, setAddUserModalOpen] = useState<boolean>(false);
  const [removeUserModalOpen, setRemoveUserModalOpen] =
    useState<boolean>(false);
  const [name, setName] = useState<string>("");
  const [isPermanent, setIsPermanent] = useState<boolean>(false);
  const [user, setUser] = useState<string>("");
  const [toDelete, setToDelete] = useState<string | null>("");
  const [userExists, setUserExists] = useState<boolean>(false);

  const [addUser] = useMutation(addUserMutation);
  const [deleteUser] = useMutation(deleteUserMutation);
  const [updateEntry] = useMutation(updateEntryMutation);
  const { data: getUsers, refetch: refetchUsers } = useQuery(getUsersQuery);
  const { data: getEntries, refetch: refetchEntries } = useQuery(
    getEntriesQuery,
    {
      variables: {
        day: date,
      },
    },
  );
  const { data: getMyEntries, refetch: refetchMyEntries } = useQuery(
    getMyEntriesQuery,
    {
      variables: {
        user: user,
      },
    },
  );

  const { setColor } = useContext(ColorContext);

  const theme = useMantineTheme();
  const { colorScheme } = useMantineColorScheme();

  useEffect(() => {
    let today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    setDate(today);
  }, []);

  useEffect(() => {
    refetchEntries();
  }, [date]);

  useEffect(() => {
    refetchMyEntries();
  }, [user]);

  const addToDate = (amount: number) => {
    const newDate = new Date(date.setDate(date.getDate() + amount));
    setDate(newDate);
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();

    if (!name) return;

    const res = await addUser({
      variables: {
        name,
        isPermanent,
      },
    });

    if (!res) return;
    if (!res.data) return;
    if (!res.data.addUser) {
      setUserExists(true);
      return;
    }

    setUserExists(false);
    setName("");
    setIsPermanent(false);
    refetchUsers();
    setAddUserModalOpen(false);
  };

  const handleDelete = async (e: FormEvent) => {
    e.preventDefault();

    if (!toDelete) return;

    const res = await deleteUser({
      variables: {
        name: toDelete,
      },
    });

    if (!res) return;
    if (!res.data) return;
    if (!res.data.deleteUser) return;
    setToDelete("");
    refetchUsers();
    setRemoveUserModalOpen(false);
  };

  const handleUpdateEntry = async (att: number) => {
    if (!user) return;
    if (!date) return;

    const res = await updateEntry({
      variables: {
        day: date,
        user,
        attendance: att,
      },
    });
    refetchEntries();
    refetchMyEntries();
  };

  const getNextColor = () => {
    const colors = Object.keys(theme.colors).slice(2);
    const index = colors.indexOf(theme.primaryColor);
    return colors[index == colors.length - 1 ? 0 : index + 1];
  };

  const accentColor =
    // @ts-ignore
    theme.colors[theme.primaryColor][theme.primaryShade["dark"]];

  const shiftedAccentColor =
    // @ts-ignore
    theme.colors[getNextColor()][theme.primaryShade["dark"]];

  const getColor = (i: number) => {
    const colors = Object.keys(theme.colors).slice(2);
    const j = (2 * i) % colors.length;
    // @ts-ignore
    return theme.colors[colors[j]][theme.primaryShade["dark"]];
  };

  const selectUser = (name: string, i: number) => {
    let themeColor;
    if (i > -1) {
      if (!getUsers) return;
      const colors = Object.keys(theme.colors).slice(2);
      const j = (2 * i) % colors.length;
      themeColor = colors[j];
    } else {
      themeColor = "blue";
    }

    setColor(themeColor);
    setUser(getUsers.getUsers.filter((u: UserType) => u.name === name)[0].name);
  };

  const getMyEntry = () => {
    if (!getEntries) return -1;
    const matches = getEntries.getEntries.filter(
      (e: EntryType) => e.user.name === user,
    );
    if (matches.length === 1) return matches[0].attendance;
    return -1;
  };

  const dayRenderer: DatePickerProps["renderDay"] = (d) => {
    let disabled;
    if (!getMyEntries) disabled = true;
    else if (!user) disabled = true;
    else if (d < new Date()) disabled = true;
    else {
      const matches = getMyEntries.getMyEntries.filter(
        (e: EntryType) =>
          new Date(e.day.date).toISOString() === d.toISOString(),
      );
      disabled = matches.length !== 0;
    }
    const day = d.getDate();
    return (
      <Indicator
        size={6}
        color={d.toISOString() === date.toISOString() ? "white" : undefined}
        offset={-5}
        disabled={disabled}
        position="bottom-center"
        zIndex={10}
      >
        <div>{day}</div>
      </Indicator>
    );
  };

  const getUsersWithoutEntry = () => {
    if (!getUsers) return [];
    if (!getEntries) return [];

    const flatmates = getUsers.getUsers
      .filter((u: UserType) => u.isPermanent)
      .map((u: UserType) => u.name);
    const usersWithEntry = getEntries.getEntries.map(
      (e: EntryType) => e.user.name,
    );
    return flatmates.filter((f: UserType) => !usersWithEntry.includes(f));
  };

  return (
    <Container size="md" px={0}>
      <Card p={0} withBorder>
        <Box
          style={{
            height: "8px",
            background: `linear-gradient(45deg, ${accentColor}, ${shiftedAccentColor})`,
          }}
        />
        <Box
          p={16}
          style={{
            backgroundColor:
              colorScheme === "dark" ? theme.colors["dark"][5] : "white",
            backgroundImage: `url("data:image/svg+xml,<svg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='${
              colorScheme === "dark" ? "%23333333" : "%23eeeeee"
            }' fill-opacity='0.75' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/></g></svg>")`,
            //borderTop: `8px solid ${accentColor}`
          }}
        >
          <h1 style={{ margin: 0, fontWeight: 800, marginTop: -8 }}>Zobig</h1>
          <Text>Am Holderbachweg 67</Text>
        </Box>
      </Card>

      <Space h="md" />

      <Card p={0} withBorder>
        <Box
          style={{
            height: "4px",
            background: `linear-gradient(45deg, ${accentColor}, ${shiftedAccentColor})`,
          }}
        />
        <Box
          p={16}
          style={{
            borderBottom: `1px solid ${
              colorScheme === "dark" ? "#444444" : "#dddddd"
            }`,
            backgroundColor:
              colorScheme === "dark"
                ? theme.colors["dark"][5]
                : theme.colors["gray"][0],
          }}
        >
          <Group justify="space-between">
            <h2 style={{ margin: 0 }}>Date</h2>

            <Group>
              <ActionIcon
                variant="transparent"
                size="xl"
                onClick={() => addToDate(-1)}
              >
                <IconArrowLeft />
              </ActionIcon>
              <ActionIcon
                variant="transparent"
                size="xl"
                onClick={() => setDate(today)}
              >
                <IconCalendar />
              </ActionIcon>
              <ActionIcon
                variant="transparent"
                size="xl"
                onClick={() => addToDate(1)}
              >
                <IconArrowRight />
              </ActionIcon>
            </Group>
          </Group>
        </Box>
        <Center p={16}>
          <DatePicker
            value={date}
            onChange={(e: Date | null) => {
              if (!e) return;
              setDate(e);
            }}
            renderDay={dayRenderer}
          />
        </Center>
      </Card>

      <Space h="md" />

      <Card p={0} withBorder>
        <Box
          style={{
            height: "4px",
            background: `linear-gradient(45deg, ${accentColor}, ${shiftedAccentColor})`,
          }}
        />
        <Box
          p={16}
          style={{
            borderBottom: `1px solid ${
              colorScheme === "dark" ? "#444444" : "#dddddd"
            }`,
            backgroundColor:
              colorScheme === "dark"
                ? theme.colors["dark"][5]
                : theme.colors["gray"][0],
          }}
        >
          <Group justify="space-between">
            <h2 style={{ margin: 0 }}>Availability</h2>

            <Group>
              {[
                {
                  value: 0,
                  icon: <IconCheck />,
                },
                {
                  value: 1,
                  icon: <IconQuestionMark />,
                },
                {
                  value: 2,
                  icon: <IconMicrowave />,
                },
                {
                  value: 3,
                  icon: <IconX />,
                },
              ].map((button) => (
                <ActionIcon
                  disabled={!user}
                  variant={
                    getMyEntry() === button.value ? "filled" : "transparent"
                  }
                  size={30}
                  key={button.value}
                  onClick={() => handleUpdateEntry(button.value)}
                >
                  {button.icon}
                </ActionIcon>
              ))}
            </Group>
          </Group>
        </Box>
        <Center p={16} style={{ flexDirection: "column" }}>
          <Box>
            {getUsers && (
              <Group justify="center">
                {getUsers.getUsers
                  .filter((u: UserType) => u.isPermanent)
                  .map((u: UserType, i: number) => (
                    <ColorSwatch
                      key={i}
                      color={getColor(i)}
                      size={64}
                      onClick={() => selectUser(u.name, i)}
                      style={{
                        border:
                          user && user == u.name
                            ? `2px solid 'white'`
                            : undefined,
                        cursor: "pointer",
                      }}
                    >
                      <Text c="white" fw={user == u.name ? 700 : 500}>
                        {u.name}
                      </Text>
                    </ColorSwatch>
                  ))}
              </Group>
            )}
          </Box>

          <Space h="md" />

          {getUsers && (
            <Group justify="center">
              {getUsers.getUsers
                .filter((u: UserType) => !u.isPermanent)
                .map((u: UserType, i: number) => (
                  <Chip
                    key={i}
                    checked={u.name === user}
                    onChange={() => selectUser(u.name, -1)}
                    value={u.name}
                  >
                    {u.name}
                  </Chip>
                ))}
            </Group>
          )}
        </Center>
      </Card>

      <Space h="md" />

      <Card p={0} withBorder>
        <Box
          style={{
            height: "4px",
            background: `linear-gradient(45deg, ${accentColor}, ${shiftedAccentColor})`,
          }}
        />
        <Box
          p={16}
          style={{
            borderBottom: `1px solid ${
              colorScheme === "dark" ? "#444444" : "#dddddd"
            }`,
            backgroundColor:
              colorScheme === "dark"
                ? theme.colors["dark"][5]
                : theme.colors["gray"][0],
          }}
        >
          <Group justify="space-between">
            <h2 style={{ margin: 0 }}>Summary</h2>

            <Group>
              <ActionIcon onClick={() => setAddUserModalOpen(true)}>
                <IconUserPlus size={20} />
              </ActionIcon>
              <ActionIcon onClick={() => setRemoveUserModalOpen(true)}>
                <IconUserMinus size={20} />
              </ActionIcon>
            </Group>
          </Group>
        </Box>
        <Box p={16}>
          {[
            {
              value: 0,
              icon: <IconCheck />,
              title: "Available",
            },
            {
              value: 1,
              icon: <IconQuestionMark />,
              title: "Not Sure",
            },
            {
              value: 2,
              icon: <IconMicrowave />,
              title: "Leftovers",
            },
            {
              value: 3,
              icon: <IconX />,
              title: "Not Available",
            },
          ].map((cat) => (
            <Alert
              variant="transparent"
              icon={cat.icon}
              title={cat.title}
              key={cat.value}
            >
              <Group>
                {getEntries &&
                  getEntries.getEntries
                    .filter((e: EntryType) => e.attendance === cat.value)
                    .map((e: EntryType, i: number) => (
                      <Badge key={i}>{e.user.name}</Badge>
                    ))}
              </Group>
            </Alert>
          ))}
          <Alert
            variant="transparent"
            icon={<IconDevicesOff />}
            title="Not Voted"
          >
            <Group>
              {getUsersWithoutEntry().map((u: string, i: number) => (
                <Badge key={i}>{u}</Badge>
              ))}
            </Group>
          </Alert>
        </Box>
      </Card>

      <Modal
        opened={addUserModalOpen}
        onClose={() => setAddUserModalOpen(false)}
        title="Add User"
      >
        <form>
          <Grid>
            <Grid.Col span={12}>
              <TextInput
                value={name}
                label="Name"
                onChange={(e) => setName(e.target.value)}
              />
              {userExists && (
                <Alert color="red" title="User Exists" mt={8}>
                  This user already exists. Please choose another name.
                </Alert>
              )}
            </Grid.Col>
            <Grid.Col span={12}>
              <Checkbox
                label="Lives Here"
                checked={isPermanent}
                onChange={(event) =>
                  setIsPermanent(event.currentTarget.checked)
                }
              />
            </Grid.Col>
            <Grid.Col span={12}>
              <Button onClick={(e) => handleSubmit(e)} type="submit">
                Submit
              </Button>
            </Grid.Col>
          </Grid>
        </form>
      </Modal>
      <Modal
        opened={removeUserModalOpen}
        onClose={() => setRemoveUserModalOpen(false)}
        title="Remove User"
      >
        <form>
          <Grid>
            <Grid.Col span={12}>
              {getUsers && (
                <Select
                  data={getUsers.getUsers.map((u: UserType) => ({
                    value: u.name,
                    label: u.name,
                  }))}
                  value={toDelete}
                  onChange={setToDelete}
                />
              )}
            </Grid.Col>
            <Grid.Col span={12}>
              <Button onClick={(e) => handleDelete(e)} type="submit">
                Submit
              </Button>
            </Grid.Col>
          </Grid>
        </form>
      </Modal>
    </Container>
  );
}
