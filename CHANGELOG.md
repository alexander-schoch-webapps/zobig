# v4.1

Add a light theme. The application is displayed in the scheme the browser / operating system is configured to display, but this can be changed in the header. Also, use a wider range of colors for the permanent flatmates to get the whole specturm. Last, add a colorful bar on top of every box to give the UI a bit more life.

# v4.0

Rewrite the complete tool with Next.js, Mantine and GraphQL. Also, rename it "Zobig".

# v3.0

Dockerize Application and deploy it to new cluster. Everything should be more reliable and faster now.

# v2.4

Fix bug that people coudn't add their vote. The problem was that the fetch API only return at max 6 results, which was not enough. This version increases this to 100.

# v2.3

Add an option "Leftovers".

# v2.2

There was a bug that a function call `e.preventDefault();` was inside of a condition to check if an added flatmate was not the empty string. Thus, if no name was provided, the page would just reload. Nothing desasterous, but still not intended.

# v2.1

There was a bug that if a guest with the same name as a flatmate is added, the database entity of this flatmate is picked and converted into a deletable guest, which is bad. Now, if e.g. "Bo" is added to the guest list, it is automatically renamed to "Bo 2: Electric Boogaloo".

# v2.0

There's a new accordion called "Iistöuige". It allows to

- Add a guest to the application. The guest list is automatically updated.
- Remove a guest from the application. The guest list is automatically updated.

Additionally, the select element "I bims" now distinguishes between flatmates and guests _via_ a value `isLivingHere` in the database.

# v1.3

Add a Chip with the current version next to the "Znacht" title. Tapping on it will open this (duh!) info dialog, which is generated automatically.

# v1.2

Sort the user entries in the "I bims" section.

- The users are now sorted alphabetically
- The Gast user will always appear in the end

# v1.1

Add a "Gast" User. This user

- can be selected in the "I bims" section
- is not visible if it hasn't voted for a day
- is visible if it has voted

# v1.0

essentially write the application
