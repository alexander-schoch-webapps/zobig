export interface UserType {
  name: string;
  isPermanent: boolean;
  entries: EntryType[];
}

export interface DayType {
  id: number;
  date: Date;
  entries: EntryType[];
}

export interface EntryType {
  id: number;
  day: DayType;
  user: UserType;
  name: string;
  attendance: number;
}
