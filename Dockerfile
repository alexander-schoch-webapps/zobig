FROM node:20

RUN mkdir -p /zobig
WORKDIR /zobig

COPY package.json /zobig
COPY package-lock.json /zobig
RUN npm install

COPY . /zobig
RUN npx prisma generate
RUN npm run build

EXPOSE 3000

CMD ["bash", "entrypoint.sh"]