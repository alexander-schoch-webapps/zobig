import { useState, ReactNode } from "react";

import {
  ActionIcon,
  AppShell,
  Badge,
  Container,
  Group,
  Modal,
  Text,
  ThemeIcon,
  useMantineTheme,
  useMantineColorScheme,
} from "@mantine/core";

import { MantineProvider, createTheme } from "@mantine/core";

import { IconSoup, IconCode, IconSun, IconMoon } from "@tabler/icons-react";

import showdown from "showdown";
import parse from "html-react-parser";

import { gql, useQuery } from "@apollo/client";

const getChangelogQuery = gql`
  query getChangelog {
    getChangelog
  }
`;

export default function Shell({ children }: { children: ReactNode }) {
  const theme = useMantineTheme();
  const { colorScheme, setColorScheme } = useMantineColorScheme();
  const [opened, setOpened] = useState(false);
  const { data: getChangelog } = useQuery(getChangelogQuery);

  const getNextColor = () => {
    const colors = Object.keys(theme.colors).slice(2);
    const index = colors.indexOf(theme.primaryColor);
    return colors[index == colors.length - 1 ? 0 : index + 1];
  };

  const getCurrentVersion = () => {
    if (!getChangelog) return null;

    const md = getChangelog.getChangelog;

    const version = md.match(/# v.*\n/)[0].match(/v.\../)[0];

    return version;
  };

  const getHtml = () => {
    if (!getChangelog) return "loading...";

    const converter = new showdown.Converter();
    return converter.makeHtml(getChangelog.getChangelog);
  };

  return (
    <AppShell header={{ height: 60 }} padding="md">
      <AppShell.Header>
        <Container size="xl" style={{ height: "100%" }}>
          <Group justify="space-between">
            <Group style={{ height: "100%" }}>
              <ThemeIcon variant="transparent" size={35} mb={4}>
                <IconSoup size={35} />
              </ThemeIcon>
              <Text
                fz={35}
                fw={800}
                variant="gradient"
                gradient={{
                  from: theme.primaryColor,
                  to: getNextColor(),
                  deg: 90,
                }}
              >
                Zobig
              </Text>
            </Group>
            <Group mt={4}>
              <ActionIcon
                component="a"
                href="https://gitlab.com/alexander-schoch-webapps/zobig"
                variant="transparent"
                target="_blank"
                mr={8}
              >
                <IconCode />
              </ActionIcon>
              <ActionIcon
                variant="transparent"
                mr={8}
                onClick={() =>
                  setColorScheme(colorScheme === "dark" ? "light" : "dark")
                }
              >
                {colorScheme === "dark" ? <IconSun /> : <IconMoon />}
              </ActionIcon>
              <Badge
                onClick={() => setOpened(true)}
                size="lg"
                style={{ cursor: "pointer" }}
              >
                {getCurrentVersion()}
              </Badge>
            </Group>
          </Group>
        </Container>
      </AppShell.Header>

      <AppShell.Main
        style={{
          backgroundColor:
            theme.colors[colorScheme === "dark" ? "dark" : "gray"][
              colorScheme === "dark" ? 8 : 1
            ],
        }}
      >
        {children}
      </AppShell.Main>

      <Modal opened={opened} onClose={() => setOpened(false)} title="Changelog">
        {parse(getHtml())}
      </Modal>
    </AppShell>
  );
}
