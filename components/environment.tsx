"use client";

import { createContext, useState, ReactNode } from "react";
export const ColorContext = createContext({ setColor: (color: string) => {} });

import "@/app/globals.css";
import "@mantine/core/styles.css";
import "@mantine/dates/styles.css";

import { MantineProvider, createTheme } from "@mantine/core";

const theme = createTheme({
  primaryColor: "blue",
  defaultRadius: "md",
  fontFamily:
    'Inter,ui-sans-serif,system-ui,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"',
});

import { InMemoryCache, ApolloProvider } from "@apollo/client";
import apolloClient from "@/lib/apollo";

import Shell from "../components/shell";

export default function Environment({ children }: { children: ReactNode }) {
  const [th, setTh] = useState(theme);

  const setColor = (color: string) => {
    const newTheme = {
      ...th,
      primaryColor: color,
    };
    setTh(newTheme);
  };

  return (
    <ApolloProvider client={apolloClient}>
      <ColorContext.Provider value={{ setColor }}>
        <MantineProvider theme={th} defaultColorScheme="auto">
          <Shell>{children}</Shell>
        </MantineProvider>
      </ColorContext.Provider>
    </ApolloProvider>
  );
}
